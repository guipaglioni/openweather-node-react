import React, { useEffect, useState } from 'react';
import axios from 'axios';
import Accordion from 'react-bootstrap/Accordion';
import 'bootstrap/dist/css/bootstrap.min.css';



function App() {
  const [backendData, setBackendData] = useState([{}])

  useEffect(() => {
    axios.get('http://localhost:5000/api').then((response) => {
      console.log(response.data);
      setBackendData(response.data);
    });
  }, []);

  return (
    <>
      <section class="vh-100" style={{"background-color": "#eee;"}}>
  <div class="container py-5 h-100">
    <div class="row d-flex justify-content-center align-items-center h-100">
      <div class="col-md-8 col-lg-6 col-xl-4">

        <div class="card" style={{"border-radius": "10px;"}}>
          <div class="bg-image ripple" data-mdb-ripple-color="light"
            style={{"border-top-left-radius": "10px", "border-top-right-radius": "10px;"}}>
            <img src="https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-weather/draw2.webp"
              class="w-100" />
            <div class="mask" style={{"background-color": "rgba(0,0,0,.45)"}}>
              <div class="d-flex justify-content-between p-4">
                <a href="#!" class="text-white"><i class="fas fa-chevron-left fa-lg"></i></a>
                <a href="#!" class="text-white"><i class="fas fa-cog fa-lg"></i></a>
              </div>
              <div class="text-center text-white">
                { backendData.weather ?
                  <p class="h3 mb-4">{backendData.weather[0].description}</p>
                
                :<><p>loading</p></> }
                <p class="h5 mb-4">{backendData.name} {backendData.sys.country}</p>
                <p class="display-2"><strong>{backendData.main.temp}</strong> </p>
              </div>
            </div>
          </div>
          <div class="card-body p-4 text-center">
            <a href="#!" class="text-body"><i class="fas fa-chevron-up fa-lg mb-4"></i></a>
            <div class="d-flex justify-content-between mb-3">
              <p class="h5 fw-normal">Umidade</p>
              <p class="h5 fw-normal"><i class="fas fa-sun pe-2"></i> {backendData.main.humidity}%</p>
            </div>
            <div class="d-flex justify-content-between" style={{"color": "#aaa;"}}>
              <p class="h5 fw-normal">Sensação térmica</p>
              <p class="h5 fw-normal"><i class="fas fa-cloud pe-2"></i> {backendData.main.feels_like}°C</p>
            </div>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>
    </>

  );
}

export default App;
